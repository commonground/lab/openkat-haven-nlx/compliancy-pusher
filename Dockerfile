FROM golang:1.19 as build

COPY . /build
WORKDIR /build

RUN go mod download
RUN CGO_ENABLED=0 go build -o /build/pusher

FROM gcr.io/distroless/static-debian11
COPY --from=build /build/pusher /pusher
CMD ["/pusher"]
