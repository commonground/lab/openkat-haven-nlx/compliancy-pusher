package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"path"

	"gitlab.com/commonground/haven/haven/haven/cli/pkg/kubernetes/models/crds/haven/v1alpha"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/kubectl/pkg/scheme"
)

type Payload struct {
	ClusterID  types.UID           `json:"clusterId"`
	Compliancy *v1alpha.Compliancy `json:"compliancy"`
}

func main() {
	endpoint := os.Getenv("PUSH_ENDPOINT")
	if endpoint == "" {
		log.Fatal("'PUSH_ENDPOINT' environment variable is not set")
	}

	homeDir, _ := os.UserHomeDir()
	kubeConfig := path.Join(homeDir, ".kube", "config")

	var (
		config *rest.Config
		err    error
	)

	// Try in-cluster or out-of-cluster configuration
	config, err = rest.InClusterConfig()
	if err != nil {
		config, err = clientcmd.BuildConfigFromFlags("", kubeConfig)
		if err != nil {
			log.Fatal(err)
		}
	}

	crdConfig := *config
	crdConfig.ContentConfig.GroupVersion = &schema.GroupVersion{Group: v1alpha.GroupName, Version: v1alpha.Version}
	crdConfig.APIPath = "/apis"
	crdConfig.NegotiatedSerializer = scheme.Codecs.WithoutConversion()
	crdConfig.UserAgent = rest.DefaultKubernetesUserAgent()

	kube, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.Fatal(err)
	}

	ctx := context.Background()

	ns, err := kube.CoreV1().Namespaces().Get(ctx, "kube-system", metav1.GetOptions{})
	if err != nil {
		log.Fatal(fmt.Errorf("failed to get 'kube-system' namespace: %w", err))
	}

	havenClient, err := rest.RESTClientFor(&crdConfig)
	if err != nil {
		log.Fatal(err)
	}

	var (
		latestCompliancy *v1alpha.Compliancy
		compliancyList   v1alpha.CompliancyList
	)

	if err := havenClient.Get().
		Resource(v1alpha.Plural).
		Do(ctx).
		Into(&compliancyList); err != nil {
		log.Fatal(err)
	}

	for _, item := range compliancyList.Items {
		if latestCompliancy == nil || item.CreationTimestamp.After(latestCompliancy.CreationTimestamp.Time) {
			latestCompliancy = &item
		}
	}

	if latestCompliancy == nil {
		log.Fatal("latest compliancy not found")
	}

	payload := Payload{
		ClusterID:  ns.UID,
		Compliancy: latestCompliancy,
	}

	data, err := json.Marshal(payload)
	if err != nil {
		log.Fatal(err)
	}

	response, err := http.DefaultClient.Post(endpoint, "application/json", bytes.NewReader(data))
	if err != nil {
		log.Fatal(err)
	}

	defer response.Body.Close()

	if response.StatusCode != http.StatusCreated {
		log.Fatal(fmt.Errorf("unexpected status code: %d", response.StatusCode))
	}

	fmt.Println("successfully pushed compliancy")
}
